# README #

Para rodar o projeto basta dar o comando npm install e após npm start.

Optei por usar o Material UI apesar de dominar css, display flex, display grid, styled-component e afins pois preferi otimizar o tempo e assim focar mais na lógica do React Hooks + Redux.

Também não dei muito foco na complexidade da API e deixei ela o mais simples possivel, assim a parte de pontuação ficou mockada no front.

Não fiz teste unitários pois não foi exigido também e levaria um pouco mais de tempo, mas estou acostumado a usar sim.

Acho que deu pra ter uma ideia de como estou acostumado a trabalhar no front com algumas coisas novas também.

