import React from 'react';
import { Provider } from 'react-redux';

import store from './store';
import Steps from './components/Steps'

const App = () => {
  return (
    <Provider store={store}>
      <Steps />
    </Provider>
  );
}

export default App;
