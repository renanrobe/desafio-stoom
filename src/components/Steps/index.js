import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  buttons: {
    marginTop: theme.spacing(5),
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),
  },
  listPizzas: {
    cursor: 'pointer'
  }
}));

const getSteps = () => {
  return ['Escolha a Massa de sua preferência', 'Selecione o Tamanho', 'Escolha um Sabor'];
}

const Steps = () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const steps = getSteps();
  
  const [activeStep, setActiveStep] = useState(0);
  const [flavorPizza, setFlavorPizza] = useState(0);

  const handleNext = () => {
    if(activeStep === 2 && flavorPizza === 0) {
      alert('Selecione um sabor')
      return false
    }else{
      setActiveStep((prevActiveStep) => prevActiveStep + 1);
    }
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleChangePasta = (event) => {
    dispatch({ type: 'ADD_PASTA', pasta: event.target.value })
  };
  
  const handleChangeSize = (event) => {
    dispatch({ type: 'ADD_SIZE', size: event.target.value })
  };
  
  const handleFlavorPizza = (name ,id) => {
    dispatch({ type: 'ADD_FLAVOR', flavor: name })
    setFlavorPizza(id);
  };

  const handleGetPizzas = ( data ) => {
    dispatch({ type: 'GET_PIZZAS', data: data })
  };

  useEffect(() => {
    const fechData = async () => {
      const response  = await fetch('https://5f0dc04711b7f60016057007.mockapi.io/pizzas');
      const dataResponse = await response.json();
      handleGetPizzas(dataResponse)
    }
    fechData()
  });

  const { solicitation, pizzas, loading } = useSelector(state => state);

  return (
    <Container maxWidth="lg">
      <div className={classes.root}>
        <Stepper activeStep={activeStep} alternativeLabel>
          {steps.map((label) => (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
            </Step>
          ))}
        </Stepper>
        <div>
          {activeStep === steps.length ? (
            <div>
              <Typography className={classes.instructions}>
                Pedido feito com sucesso!<br />
                Massa: {solicitation.pasta}<br />
                Tamanho: {solicitation.size}<br />
                Sabor: {solicitation.flavor}<br />
                Você acumulou: {solicitation.points}
              </Typography>
            </div>
          ) : (
            <FormControl component="fieldset" className={classes.instructions}>
              { loading ? <p>Carregando...</p> :
              <div>
                {activeStep === 0 && 
                  <RadioGroup aria-label="gender" name="pasta" value={solicitation.pasta} onChange={handleChangePasta}>
                    <FormControlLabel value="Fina" control={<Radio />} label="Fina" />
                    <FormControlLabel value="Media" control={<Radio />} label="Media" />
                    <FormControlLabel value="Grossa" control={<Radio />} label="Grossa" />
                  </RadioGroup>
                }
                {activeStep === 1 &&
                  <RadioGroup aria-label="gender" name="size" value={solicitation.size} onChange={handleChangeSize}>
                    <FormControlLabel value="Broto" control={<Radio />} label="Brotinho (4 pedaços)" />
                    <FormControlLabel value="Grande" control={<Radio />} label="Grande (8 pedaços)" />
                    <FormControlLabel value="Giga" control={<Radio />} label="Giga (12 pedaços)" />
                  </RadioGroup>
                }
                {activeStep === 2 && 
                  <List>
                    {pizzas.map(item =>
                      <ListItem
                        key={item.id}
                        selected={flavorPizza === item.id}
                        className={classes.listPizzas}
                        onClick={() => handleFlavorPizza(item, item.id)}
                      >
                        <ListItemText
                          primary={`${item.name} - ${item.price} ${item.promotion && '(PROMOÇÃO)'}`}
                          secondary={item.ingredients}
                        />
                      </ListItem>
                    )}
                  </List>
                }
              </div>}

              <div className={classes.buttons}>
                <Button
                  disabled={activeStep === 0}
                  onClick={handleBack}
                  className={classes.backButton}
                >
                  Voltar
                </Button>
                <Button variant="contained" color="primary" onClick={handleNext}>
                  {activeStep === steps.length - 1 ? 'Finalizar' : 'Próximo'}
                </Button>
              </div>
            </FormControl>
          )}
        </div>
      </div>
    </Container>
  );
}

export default Steps