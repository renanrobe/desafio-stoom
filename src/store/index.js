import { createStore } from 'redux'

const INITIAL_STATE = {
  pizzas: [],
  loading: true,
  solicitation: {
    pasta: 'Media',
    size: 'Grande',
    flavor: '',
    points: ''
  }
};

const appReducer = (state = INITIAL_STATE, ACTION) => {
  switch(ACTION.type) {
    case 'GET_PIZZAS':
      return {
        ...state,
        loading: false,
        pizzas: ACTION.data
      };
    case 'ADD_PASTA':
      return {
        ...state,
        solicitation: {
          ...state.solicitation,
          pasta: ACTION.pasta
        }
      };
    case 'ADD_SIZE':
      return { 
        ...state,
        solicitation: {
          ...state.solicitation,
          size: ACTION.size
        }
      };
    case 'ADD_FLAVOR':
      return {
        ...state,
        solicitation: {
          ...state.solicitation,
          flavor: ACTION.flavor.name,
          points: ACTION.flavor.promotion ? 10 : 0
        }
      };
    default:
      return state;
  }
}
const store = createStore(appReducer);

export default store;